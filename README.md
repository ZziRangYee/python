# Python

[파이썬]
- 인강보기
- 인프런 : https://www.inflearn.com/course/%EB%A8%B8%EC%8B%A0%EB%9F%AC%EB%8B%9D%EC%9D%B4%EB%A1%A0-%ED%8C%8C%EC%9D%B4%EC%8D%AC%EC%8B%A4%EC%8A%B5/ 

참고 URL : 
KNN : https://github.com/minsuk-heo/machinelearning/blob/master/machineLearningInAction/kNN.py

=== 18.10.08 ===
- 세팅하고 최근접이웃알고리즘까지 듣기

=== 18.10.16 ===
- 의사결정 트리까지 듣고 공부해오기.(ID3 알고리즘 수학적 접근)

=== 18.11.13 ===
- 나이브 베이즈(Naive Bayes) 분류까지 듣고 공부해오기.(베이즈 정리 (Bayes’ Theorem) 쉽게 이해하기)

=== 19.03.07 ===
- 인프런 : https://www.inflearn.com/course/%EC%9B%B9-%ED%81%AC%EB%A1%A4%EB%A7%81web-crawling-%EC%96%B4%ED%94%8C%EB%A6%AC%EC%BC%80%EC%9D%B4%EC%85%98-%EB%A7%8C%EB%93%A4%EA%B8%B0/
- 19.03.14일까지 3강 까지 듣고 개발환경 구축해서 오기.

=== 19.03.21 ===
 - https://www.inflearn.com/course/%EC%9B%B9-%ED%81%AC%EB%A1%A4%EB%A7%81web-crawling-%EC%96%B4%ED%94%8C%EB%A6%AC%EC%BC%80%EC%9D%B4%EC%85%98-%EB%A7%8C%EB%93%A4%EA%B8%B0/05-%EC%9B%B9-%EB%8D%B0%EC%9D%B4%ED%84%B0-%EC%A0%80%EC%9E%91%EA%B6%8C-%EB%B0%8F-%EC%82%AC%EC%9D%B4%ED%8A%B8-%EC%A0%95%EC%B1%85/
 
```
19.04.01
https://www.youtube.com/watch?v=rtwtOcfYKqc&t=1667s
```